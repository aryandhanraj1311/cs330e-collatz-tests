#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from pickle import NONE
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "34 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  34)
        self.assertEqual(j, 100)

    def test_read_3(self):
        s = "3 999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 999)

    def test_read_4(self):
        s = "345 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  345)
        self.assertEqual(j, 10)

    def test_read_5(self):
        s = "34 34\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  34)
        self.assertEqual(j, 34)

    def test_read_6(self):
        s = "3 99999\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  3)
        self.assertEqual(j, 99999)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10, cache={})
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200, cache={})
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210, cache={})
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000, cache={})
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(34, 34, cache={})
        self.assertEqual(v, 14)

    def test_eval_6(self):
        v = collatz_eval(1, 1, cache={})
        self.assertEqual(v, 1)

    def test_eval_7(self):
        v = collatz_eval(567,90, cache={})
        self.assertEqual(v, 144)

    def test_eval_8(self):
        v = collatz_eval(9, 99999, cache={})
        self.assertEqual(v, 351)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 569, 10, 144)
        self.assertEqual(w.getvalue(), "569 10 144\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 123, 10, 120)
        self.assertEqual(w.getvalue(), "123 10 120\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 0, 100, 20)
        self.assertEqual(w.getvalue(), "0 100 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_1(self):
        r = StringIO("1 1\n34 34\n201 210\n567 90\n9 99999\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n34 34 14\n201 210 89\n567 90 144\n9 99999 351\n")
    
    def test_solve_2(self):
        r = StringIO("45 30\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "45 30 110\n")

    def test_solve_3(self):
        r = StringIO("777 778\n2 1\n778 777\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "777 778 122\n2 1 2\n778 777 122\n")

    def test_solve_4(self):
        r = StringIO("\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(),"")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
